import { __decorate, __param } from "tslib";
import { Inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { ProfileModel } from '../models/profile.model';
import { PROFILE_REPOSITORY } from '../repositories/IProfile.repository';
import * as i0 from "@angular/core";
import * as i1 from "../repositories/IProfile.repository";
let ProfileService = class ProfileService {
    constructor(repository) {
        this.repository = repository;
    }
    getProfile() {
        return this.repository.getProfile().pipe(map((response) => {
            return ProfileModel.toModel(response.data);
        }), catchError(error => {
            throw error;
        }));
    }
    updateProfile(profileForm) {
        return this.repository.updateProfile(profileForm).pipe(map((response) => {
            return new ProfileModel({
                activeNewsletter: null,
                businessName: response.data.business_name,
                createdAt: null,
                clientEmail: null,
                clientId: null,
                name: response.data.name,
                phoneNumber: null,
                userEmail: response.data.email,
                userId: +response.data.id,
            });
        }), catchError(error => {
            throw error;
        }));
    }
};
ProfileService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [PROFILE_REPOSITORY,] }] }
];
ProfileService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProfileService_Factory() { return new ProfileService(i0.ɵɵinject(i1.PROFILE_REPOSITORY)); }, token: ProfileService, providedIn: "root" });
ProfileService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(PROFILE_REPOSITORY))
], ProfileService);
export { ProfileService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcHJvZmlsZS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL3Byb2ZpbGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdkQsT0FBTyxFQUtILGtCQUFrQixFQUNyQixNQUFNLHFDQUFxQyxDQUFDOzs7QUFNN0MsSUFBYSxjQUFjLEdBQTNCLE1BQWEsY0FBYztJQUV2QixZQUN3QyxVQUE4QjtRQUE5QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtJQUNsRSxDQUFDO0lBRUwsVUFBVTtRQUNOLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxJQUFJLENBQ3BDLEdBQUcsQ0FBQyxDQUFDLFFBQXFELEVBQUUsRUFBRTtZQUMxRCxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNmLE1BQU0sS0FBSyxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxDQUNMLENBQUM7SUFDTixDQUFDO0lBRUQsYUFBYSxDQUFDLFdBQWtDO1FBQzVDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUNsRCxHQUFHLENBQUMsQ0FBQyxRQUF3RCxFQUFFLEVBQUU7WUFFN0QsT0FBTyxJQUFJLFlBQVksQ0FBQztnQkFDcEIsZ0JBQWdCLEVBQUUsSUFBSTtnQkFDdEIsWUFBWSxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYTtnQkFDekMsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsV0FBVyxFQUFFLElBQUk7Z0JBQ2pCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUk7Z0JBQ3hCLFdBQVcsRUFBRSxJQUFJO2dCQUNqQixTQUFTLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLO2dCQUM5QixNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7YUFDNUIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLEVBQ0YsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2YsTUFBTSxLQUFLLENBQUM7UUFDaEIsQ0FBQyxDQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Q0FDSixDQUFBOzs0Q0FuQ1EsTUFBTSxTQUFDLGtCQUFrQjs7O0FBSHJCLGNBQWM7SUFIMUIsVUFBVSxDQUFDO1FBQ1IsVUFBVSxFQUFFLE1BQU07S0FDckIsQ0FBQztJQUlPLFdBQUEsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7R0FIdEIsY0FBYyxDQXNDMUI7U0F0Q1ksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSUh0dHBCYXNpY1Jlc3BvbnNlIH0gZnJvbSAnQGJveHgvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBQcm9maWxlTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvcHJvZmlsZS5tb2RlbCc7XG5pbXBvcnQge1xuICAgIElHZXRQcm9maWxlRGF0YVJlc3BvbnNlLFxuICAgIElQcm9maWxlUmVwb3NpdG9yeSxcbiAgICBJVXBkYXRlUHJvZmlsZURhdGFSZXNwb25zZSxcbiAgICBJVXBkYXRlUHJvZmlsZVBheWxvYWQsXG4gICAgUFJPRklMRV9SRVBPU0lUT1JZXG59IGZyb20gJy4uL3JlcG9zaXRvcmllcy9JUHJvZmlsZS5yZXBvc2l0b3J5JztcbmltcG9ydCB7IElQcm9maWxlU2VydmljZSB9IGZyb20gJy4vSVByb2ZpbGUuc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUHJvZmlsZVNlcnZpY2UgaW1wbGVtZW50cyBJUHJvZmlsZVNlcnZpY2U8UHJvZmlsZU1vZGVsPntcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBASW5qZWN0KFBST0ZJTEVfUkVQT1NJVE9SWSkgcHJpdmF0ZSByZXBvc2l0b3J5OiBJUHJvZmlsZVJlcG9zaXRvcnksXG4gICAgKSB7IH1cblxuICAgIGdldFByb2ZpbGUoKTogT2JzZXJ2YWJsZTxQcm9maWxlTW9kZWw+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVwb3NpdG9yeS5nZXRQcm9maWxlKCkucGlwZShcbiAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IElIdHRwQmFzaWNSZXNwb25zZTxJR2V0UHJvZmlsZURhdGFSZXNwb25zZT4pID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gUHJvZmlsZU1vZGVsLnRvTW9kZWwocmVzcG9uc2UuZGF0YSk7XG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIHRocm93IGVycm9yO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICB1cGRhdGVQcm9maWxlKHByb2ZpbGVGb3JtOiBJVXBkYXRlUHJvZmlsZVBheWxvYWQpOiBPYnNlcnZhYmxlPFByb2ZpbGVNb2RlbD4ge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXBvc2l0b3J5LnVwZGF0ZVByb2ZpbGUocHJvZmlsZUZvcm0pLnBpcGUoXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBJSHR0cEJhc2ljUmVzcG9uc2U8SVVwZGF0ZVByb2ZpbGVEYXRhUmVzcG9uc2U+KSA9PiB7XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb2ZpbGVNb2RlbCh7XG4gICAgICAgICAgICAgICAgICAgIGFjdGl2ZU5ld3NsZXR0ZXI6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIGJ1c2luZXNzTmFtZTogcmVzcG9uc2UuZGF0YS5idXNpbmVzc19uYW1lLFxuICAgICAgICAgICAgICAgICAgICBjcmVhdGVkQXQ6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIGNsaWVudEVtYWlsOiBudWxsLFxuICAgICAgICAgICAgICAgICAgICBjbGllbnRJZDogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogcmVzcG9uc2UuZGF0YS5uYW1lLFxuICAgICAgICAgICAgICAgICAgICBwaG9uZU51bWJlcjogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgdXNlckVtYWlsOiByZXNwb25zZS5kYXRhLmVtYWlsLFxuICAgICAgICAgICAgICAgICAgICB1c2VySWQ6ICtyZXNwb25zZS5kYXRhLmlkLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSksXG4gICAgICAgICAgICBjYXRjaEVycm9yKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICk7XG4gICAgfVxufVxuIl19