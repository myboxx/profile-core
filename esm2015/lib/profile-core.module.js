var ProfileCoreModule_1;
import { __decorate } from "tslib";
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PROFILE_REPOSITORY } from './repositories/IProfile.repository';
import { ProfileRepository } from './repositories/profile.repository';
import { PROFILE_SERVICE } from './services/IProfile.service';
import { ProfileService } from './services/profile.service';
import { ProfileEffects } from './state/profile.effects';
import { profileReducer } from './state/profile.reducer';
import { ProfileStore } from './state/profile.store';
let ProfileCoreModule = ProfileCoreModule_1 = class ProfileCoreModule {
    static forRoot(config) {
        return {
            ngModule: ProfileCoreModule_1,
            providers: [
                { provide: PROFILE_SERVICE, useClass: ProfileService },
                { provide: PROFILE_REPOSITORY, useClass: ProfileRepository },
                ...config.providers,
                ProfileStore
            ]
        };
    }
};
ProfileCoreModule = ProfileCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('profile', profileReducer),
            EffectsModule.forFeature([ProfileEffects]),
        ],
        exports: []
    })
], ProfileCoreModule);
export { ProfileCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS1jb3JlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3Byb2ZpbGUtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9wcm9maWxlLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QixRQUFRLEVBQVksTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFtQnJELElBQWEsaUJBQWlCLHlCQUE5QixNQUFhLGlCQUFpQjtJQUMxQixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQThCO1FBQ3pDLE9BQU87WUFDSCxRQUFRLEVBQUUsbUJBQWlCO1lBQzNCLFNBQVMsRUFBRTtnQkFDUCxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLGNBQWMsRUFBRTtnQkFDdEQsRUFBRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixFQUFFO2dCQUM1RCxHQUFHLE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixZQUFZO2FBQ2Y7U0FDSixDQUFDO0lBQ04sQ0FBQztDQUNILENBQUE7QUFaVyxpQkFBaUI7SUFiN0IsUUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFLEVBRWI7UUFDRCxPQUFPLEVBQUU7WUFDUCxnQkFBZ0I7WUFDaEIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDO1lBQ2pELGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUMzQztRQUNELE9BQU8sRUFBRSxFQUVSO0tBQ0YsQ0FBQztHQUNXLGlCQUFpQixDQVk1QjtTQVpXLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEVmZmVjdHNNb2R1bGUgfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcbmltcG9ydCB7IFN0b3JlTW9kdWxlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgUFJPRklMRV9SRVBPU0lUT1JZIH0gZnJvbSAnLi9yZXBvc2l0b3JpZXMvSVByb2ZpbGUucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBQcm9maWxlUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL3Byb2ZpbGUucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBQUk9GSUxFX1NFUlZJQ0UgfSBmcm9tICcuL3NlcnZpY2VzL0lQcm9maWxlLnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvZmlsZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3Byb2ZpbGUuc2VydmljZSc7XG5pbXBvcnQgeyBQcm9maWxlRWZmZWN0cyB9IGZyb20gJy4vc3RhdGUvcHJvZmlsZS5lZmZlY3RzJztcbmltcG9ydCB7IHByb2ZpbGVSZWR1Y2VyIH0gZnJvbSAnLi9zdGF0ZS9wcm9maWxlLnJlZHVjZXInO1xuaW1wb3J0IHsgUHJvZmlsZVN0b3JlIH0gZnJvbSAnLi9zdGF0ZS9wcm9maWxlLnN0b3JlJztcblxuaW50ZXJmYWNlIE1vZHVsZU9wdGlvbnNJbnRlcmZhY2Uge1xuICAgIHByb3ZpZGVyczogUHJvdmlkZXJbXTtcbn1cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG5cbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgU3RvcmVNb2R1bGUuZm9yRmVhdHVyZSgncHJvZmlsZScsIHByb2ZpbGVSZWR1Y2VyKSxcbiAgICBFZmZlY3RzTW9kdWxlLmZvckZlYXR1cmUoW1Byb2ZpbGVFZmZlY3RzXSksXG4gIF0sXG4gIGV4cG9ydHM6IFtcblxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFByb2ZpbGVDb3JlTW9kdWxlIHtcbiAgICBzdGF0aWMgZm9yUm9vdChjb25maWc6IE1vZHVsZU9wdGlvbnNJbnRlcmZhY2UpOiBNb2R1bGVXaXRoUHJvdmlkZXJzPFByb2ZpbGVDb3JlTW9kdWxlPiB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBuZ01vZHVsZTogUHJvZmlsZUNvcmVNb2R1bGUsXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IFBST0ZJTEVfU0VSVklDRSwgdXNlQ2xhc3M6IFByb2ZpbGVTZXJ2aWNlIH0sXG4gICAgICAgICAgICAgICAgeyBwcm92aWRlOiBQUk9GSUxFX1JFUE9TSVRPUlksIHVzZUNsYXNzOiBQcm9maWxlUmVwb3NpdG9yeSB9LFxuICAgICAgICAgICAgICAgIC4uLmNvbmZpZy5wcm92aWRlcnMsXG4gICAgICAgICAgICAgICAgUHJvZmlsZVN0b3JlXG4gICAgICAgICAgICBdXG4gICAgICAgIH07XG4gICAgfVxuIH1cbiJdfQ==