import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfileModel } from '../models/profile.model';
import { IUpdateProfilePayload } from '../repositories/IProfile.repository';
export interface IProfileService<T1> {
    getProfile(): Observable<T1>;
    updateProfile(profileForm: IUpdateProfilePayload): Observable<T1>;
}
export declare const PROFILE_SERVICE: InjectionToken<IProfileService<ProfileModel>>;
