import { Observable } from 'rxjs';
import { ProfileModel } from '../models/profile.model';
import { IProfileRepository, IUpdateProfilePayload } from '../repositories/IProfile.repository';
import { IProfileService } from './IProfile.service';
export declare class ProfileService implements IProfileService<ProfileModel> {
    private repository;
    constructor(repository: IProfileRepository);
    getProfile(): Observable<ProfileModel>;
    updateProfile(profileForm: IUpdateProfilePayload): Observable<ProfileModel>;
}
