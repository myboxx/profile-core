import { HttpClient } from '@angular/common/http';
import { AbstractAppConfigService, IHttpBasicResponse } from '@boxx/core';
import { Observable } from 'rxjs';
import { IGetProfileDataResponse, IProfileRepository, IUpdateProfileDataResponse, IUpdateProfilePayload } from './IProfile.repository';
export declare class ProfileRepository implements IProfileRepository {
    private appSettings;
    private httpClient;
    constructor(appSettings: AbstractAppConfigService, httpClient: HttpClient);
    getProfile(): Observable<IHttpBasicResponse<IGetProfileDataResponse>>;
    updateProfile(profileForm: IUpdateProfilePayload): Observable<IHttpBasicResponse<IUpdateProfileDataResponse>>;
    private getBaseUrl;
}
