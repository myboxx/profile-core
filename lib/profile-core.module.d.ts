import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class ProfileCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<ProfileCoreModule>;
}
export {};
