import { Actions } from '@ngrx/effects';
import { ProfileModel } from '../models/profile.model';
import { IProfileService } from '../services/IProfile.service';
import * as ProfileActions from './profile.actions';
export declare class ProfileEffects {
    private actions$;
    private service;
    loadProfile$: import("rxjs").Observable<({
        payload: ProfileModel;
    } & import("@ngrx/store/src/models").TypedAction<ProfileActions.ProfileActionTypes.LoadProfileSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<ProfileActions.ProfileActionTypes.LoadProfileFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    updateProfile$: import("rxjs").Observable<({
        response: ProfileModel;
    } & import("@ngrx/store/src/models").TypedAction<ProfileActions.ProfileActionTypes.UpdateProfileSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<ProfileActions.ProfileActionTypes.UpdateProfileFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, service: IProfileService<ProfileModel>);
}
