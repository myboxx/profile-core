import * as fromProfileReducer from './profile.reducer';
export declare const getProfileState: import("@ngrx/store").MemoizedSelector<object, fromProfileReducer.ProfileState, import("@ngrx/store").DefaultProjectorFn<fromProfileReducer.ProfileState>>;
export declare const getProfilePageState: import("@ngrx/store").MemoizedSelector<object, fromProfileReducer.ProfileState, import("@ngrx/store").DefaultProjectorFn<fromProfileReducer.ProfileState>>;
export declare const stateGetProfile: (state: fromProfileReducer.ProfileState) => import("../models/profile.model").ProfileModel;
export declare const stateGetError: (state: fromProfileReducer.ProfileState) => import("../core/IStateErrorSuccess").IProfileStateError;
export declare const getIsLoading: import("@ngrx/store").MemoizedSelector<object, boolean, import("@ngrx/store").DefaultProjectorFn<boolean>>;
export declare const getProfile: import("@ngrx/store").MemoizedSelector<object, import("../models/profile.model").ProfileModel, import("@ngrx/store").DefaultProjectorFn<import("../models/profile.model").ProfileModel>>;
export declare const getError: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IProfileStateError, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IProfileStateError>>;
export declare const getSuccess: import("@ngrx/store").MemoizedSelector<object, import("../core/IStateErrorSuccess").IProfileStateSuccess, import("@ngrx/store").DefaultProjectorFn<import("../core/IStateErrorSuccess").IProfileStateSuccess>>;
