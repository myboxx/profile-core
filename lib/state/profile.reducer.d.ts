import { Action } from '@ngrx/store';
import { IProfileStateError, IProfileStateSuccess } from '../core/IStateErrorSuccess';
import { ProfileModel } from '../models/profile.model';
export interface ProfileState {
    isLoading: boolean;
    profile: ProfileModel;
    error: IProfileStateError;
    success: IProfileStateSuccess;
}
export declare const initialState: ProfileState;
export declare function profileReducer(state: ProfileState | undefined, action: Action): ProfileState;
