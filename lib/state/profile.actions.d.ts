import { ProfileModel } from '../models/profile.model';
export declare enum ProfileActionTypes {
    LoadProfileBegin = "[PROFILE] Load Profile Begin",
    LoadProfileSuccess = "[PROFILE] Load Profile Success",
    LoadProfileFail = "[PROFILE] Load Profile Fail",
    UpdateProfileBegin = "[PROFILE] Update Profile Begin",
    UpdateProfileSuccess = "[PROFILE] Update Profile Success",
    UpdateProfileFail = "[PROFILE] Update Profile Fail"
}
export declare const LoadProfileBeginAction: import("@ngrx/store").ActionCreator<ProfileActionTypes.LoadProfileBegin, () => import("@ngrx/store/src/models").TypedAction<ProfileActionTypes.LoadProfileBegin>>;
export declare const LoadProfileSuccessAction: import("@ngrx/store").ActionCreator<ProfileActionTypes.LoadProfileSuccess, (props: {
    payload: ProfileModel;
}) => {
    payload: ProfileModel;
} & import("@ngrx/store/src/models").TypedAction<ProfileActionTypes.LoadProfileSuccess>>;
export declare const LoadProfileFailAction: import("@ngrx/store").ActionCreator<ProfileActionTypes.LoadProfileFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ProfileActionTypes.LoadProfileFail>>;
export declare const UpdateProfileBeginAction: import("@ngrx/store").ActionCreator<ProfileActionTypes.UpdateProfileBegin, (props: {
    profileForm: {
        name: string;
        email: string;
    };
}) => {
    profileForm: {
        name: string;
        email: string;
    };
} & import("@ngrx/store/src/models").TypedAction<ProfileActionTypes.UpdateProfileBegin>>;
export declare const UpdateProfileSuccessAction: import("@ngrx/store").ActionCreator<ProfileActionTypes.UpdateProfileSuccess, (props: {
    response: ProfileModel;
}) => {
    response: ProfileModel;
} & import("@ngrx/store/src/models").TypedAction<ProfileActionTypes.UpdateProfileSuccess>>;
export declare const UpdateProfileFailAction: import("@ngrx/store").ActionCreator<ProfileActionTypes.UpdateProfileFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<ProfileActionTypes.UpdateProfileFail>>;
