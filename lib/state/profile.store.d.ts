import { Store } from '@ngrx/store';
import * as fromReducer from './profile.reducer';
export declare class ProfileStore {
    private store;
    constructor(store: Store<fromReducer.ProfileState>);
    get Loading$(): import("rxjs").Observable<boolean>;
    get Error$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IProfileStateError>;
    get Success$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IProfileStateSuccess>;
    LoadProfile(): void;
    get Profile$(): import("rxjs").Observable<import("../models/profile.model").ProfileModel>;
    UpdateProfile(profileForm: {
        name: string;
        email: string;
    }): void;
}
