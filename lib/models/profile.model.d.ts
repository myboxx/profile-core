import { IGetProfileDataResponse } from '../repositories/IProfile.repository';
export declare class ProfileModel implements IProfileModelProps {
    constructor(data: IProfileModelProps);
    activeNewsletter: boolean;
    businessName: string;
    createdAt: string;
    clientEmail: string;
    clientId: number;
    name: string;
    phoneNumber: string;
    userEmail: string;
    userId: number;
    static toModel(data: IGetProfileDataResponse): ProfileModel;
    static empty(): ProfileModel;
}
export interface IProfileModelProps {
    activeNewsletter: boolean;
    businessName: string;
    createdAt: string;
    clientEmail: string;
    clientId: number;
    name: string;
    phoneNumber: string;
    userEmail: string;
    userId: number;
}
