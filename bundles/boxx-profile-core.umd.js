(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common/http'), require('@angular/core'), require('@ngrx/effects'), require('@ngrx/store'), require('@boxx/core'), require('rxjs/operators'), require('rxjs')) :
    typeof define === 'function' && define.amd ? define('@boxx/profile-core', ['exports', '@angular/common/http', '@angular/core', '@ngrx/effects', '@ngrx/store', '@boxx/core', 'rxjs/operators', 'rxjs'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['profile-core'] = {}), global.ng.common.http, global.ng.core, global['ngrx-effects'], global['ngrx-store'], global['boxx-core'], global.rxjs.operators, global.rxjs));
}(this, (function (exports, http, core, effects, store, core$1, operators, rxjs) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var PROFILE_REPOSITORY = new core.InjectionToken('profileRepository');

    var ProfileRepository = /** @class */ (function () {
        function ProfileRepository(appSettings, httpClient) {
            this.appSettings = appSettings;
            this.httpClient = httpClient;
        }
        ProfileRepository.prototype.getProfile = function () {
            return this.httpClient.get(this.getBaseUrl() + "/get_profile");
        };
        ProfileRepository.prototype.updateProfile = function (profileForm) {
            var params = new http.HttpParams();
            for (var key in profileForm) {
                if (profileForm.hasOwnProperty(key)) {
                    params = params.append(key, profileForm[key]);
                }
            }
            var body = params.toString();
            return this.httpClient.post(this.getBaseUrl() + "/update", body);
        };
        ProfileRepository.prototype.getBaseUrl = function () {
            return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/profile";
        };
        ProfileRepository.ctorParameters = function () { return [
            { type: core$1.AbstractAppConfigService, decorators: [{ type: core.Inject, args: [core$1.APP_CONFIG_SERVICE,] }] },
            { type: http.HttpClient }
        ]; };
        ProfileRepository = __decorate([
            core.Injectable(),
            __param(0, core.Inject(core$1.APP_CONFIG_SERVICE))
        ], ProfileRepository);
        return ProfileRepository;
    }());

    var PROFILE_SERVICE = new core.InjectionToken('profileService');

    var ProfileModel = /** @class */ (function () {
        function ProfileModel(data) {
            this.businessName = data.businessName;
            this.activeNewsletter = data.activeNewsletter;
            this.createdAt = data.createdAt;
            this.clientEmail = data.clientEmail;
            this.clientId = data.clientId;
            this.name = data.name;
            this.phoneNumber = data.phoneNumber;
            this.userEmail = data.userEmail;
            this.userId = data.userId;
        }
        ProfileModel.toModel = function (data) {
            return new ProfileModel({
                activeNewsletter: data.active_newsletter === '1',
                businessName: data.business_name,
                createdAt: data.created_at,
                clientEmail: data.client_email,
                clientId: +data.client_id,
                name: data.name,
                phoneNumber: data.phone_number !== 'null' ? data.phone_number : null,
                userEmail: data.user_email,
                userId: +data.user_id
            });
        };
        ProfileModel.empty = function () {
            return new ProfileModel({
                activeNewsletter: null,
                businessName: null,
                createdAt: null,
                clientEmail: null,
                clientId: null,
                name: null,
                phoneNumber: null,
                userEmail: null,
                userId: null
            });
        };
        return ProfileModel;
    }());

    var ProfileService = /** @class */ (function () {
        function ProfileService(repository) {
            this.repository = repository;
        }
        ProfileService.prototype.getProfile = function () {
            return this.repository.getProfile().pipe(operators.map(function (response) {
                return ProfileModel.toModel(response.data);
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ProfileService.prototype.updateProfile = function (profileForm) {
            return this.repository.updateProfile(profileForm).pipe(operators.map(function (response) {
                return new ProfileModel({
                    activeNewsletter: null,
                    businessName: response.data.business_name,
                    createdAt: null,
                    clientEmail: null,
                    clientId: null,
                    name: response.data.name,
                    phoneNumber: null,
                    userEmail: response.data.email,
                    userId: +response.data.id,
                });
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        ProfileService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [PROFILE_REPOSITORY,] }] }
        ]; };
        ProfileService.ɵprov = core.ɵɵdefineInjectable({ factory: function ProfileService_Factory() { return new ProfileService(core.ɵɵinject(PROFILE_REPOSITORY)); }, token: ProfileService, providedIn: "root" });
        ProfileService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(0, core.Inject(PROFILE_REPOSITORY))
        ], ProfileService);
        return ProfileService;
    }());


    (function (ProfileActionTypes) {
        ProfileActionTypes["LoadProfileBegin"] = "[PROFILE] Load Profile Begin";
        ProfileActionTypes["LoadProfileSuccess"] = "[PROFILE] Load Profile Success";
        ProfileActionTypes["LoadProfileFail"] = "[PROFILE] Load Profile Fail";
        ProfileActionTypes["UpdateProfileBegin"] = "[PROFILE] Update Profile Begin";
        ProfileActionTypes["UpdateProfileSuccess"] = "[PROFILE] Update Profile Success";
        ProfileActionTypes["UpdateProfileFail"] = "[PROFILE] Update Profile Fail";
    })(exports.ProfileActionTypes || (exports.ProfileActionTypes = {}));
    // GET
    var LoadProfileBeginAction = store.createAction(exports.ProfileActionTypes.LoadProfileBegin);
    var LoadProfileSuccessAction = store.createAction(exports.ProfileActionTypes.LoadProfileSuccess, store.props());
    var LoadProfileFailAction = store.createAction(exports.ProfileActionTypes.LoadProfileFail, store.props());
    // UPDATE
    var UpdateProfileBeginAction = store.createAction(exports.ProfileActionTypes.UpdateProfileBegin, store.props());
    var UpdateProfileSuccessAction = store.createAction(exports.ProfileActionTypes.UpdateProfileSuccess, store.props());
    var UpdateProfileFailAction = store.createAction(exports.ProfileActionTypes.UpdateProfileFail, store.props());

    var ProfileEffects = /** @class */ (function () {
        function ProfileEffects(actions$, service) {
            var _this = this;
            this.actions$ = actions$;
            this.service = service;
            this.loadProfile$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ProfileActionTypes.LoadProfileBegin), operators.switchMap(function () {
                return _this.service.getProfile().pipe(operators.map(function (profile) { return LoadProfileSuccessAction({ payload: profile }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t load Profile');
                    return rxjs.of(LoadProfileFailAction({ errors: error }));
                }));
            })); });
            this.updateProfile$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.ProfileActionTypes.UpdateProfileBegin), operators.switchMap(function (action) {
                return _this.service.updateProfile(action.profileForm).pipe(operators.map(function (response) { return UpdateProfileSuccessAction({ response: response }); }), operators.catchError(function (error) {
                    console.error('Couldn\'t UPDATE Profile', error);
                    return rxjs.of(UpdateProfileFailAction({ errors: error }));
                }));
            })); });
        }
        ProfileEffects.ctorParameters = function () { return [
            { type: effects.Actions },
            { type: undefined, decorators: [{ type: core.Inject, args: [PROFILE_SERVICE,] }] }
        ]; };
        ProfileEffects = __decorate([
            core.Injectable(),
            __param(1, core.Inject(PROFILE_SERVICE))
        ], ProfileEffects);
        return ProfileEffects;
    }());

    var initialState = {
        isLoading: false,
        profile: ProfileModel.empty(),
        error: null,
        success: null
    };
    var ɵ0 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { profile: action.payload, isLoading: false })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { profile: action.response, isLoading: false, success: { after: getSuccessActionType(action.type) } })); };
    var reducer = store.createReducer(initialState, store.on(LoadProfileBeginAction, UpdateProfileBeginAction, ɵ0), store.on(LoadProfileSuccessAction, ɵ1), store.on(LoadProfileFailAction, UpdateProfileFailAction, ɵ2), store.on(UpdateProfileSuccessAction, ɵ3));
    function getErrorActionType(type) {
        var action = 'UNKNOWN';
        switch (type) {
            case exports.ProfileActionTypes.LoadProfileFail:
                action = 'GET';
                break;
            case exports.ProfileActionTypes.UpdateProfileFail:
                action = 'UPDATE';
                break;
        }
        return action;
    }
    function getSuccessActionType(type) {
        var action = 'UNKNOWN';
        switch (type) {
            case exports.ProfileActionTypes.LoadProfileSuccess:
                action = 'GET';
                break;
            case exports.ProfileActionTypes.UpdateProfileSuccess:
                action = 'UPDATE';
                break;
        }
        return action;
    }
    function profileReducer(state, action) {
        return reducer(state, action);
    }

    var getProfileState = store.createFeatureSelector('profile');
    var ɵ0$1 = function (state) { return state; };
    var getProfilePageState = store.createSelector(getProfileState, ɵ0$1);
    var stateGetIsLoading = function (state) { return state.isLoading; };
    var ɵ1$1 = stateGetIsLoading;
    var stateGetProfile = function (state) { return state.profile; };
    var stateGetError = function (state) { return state.error; };
    var getIsLoading = store.createSelector(getProfilePageState, stateGetIsLoading);
    var getProfile = store.createSelector(getProfilePageState, stateGetProfile);
    var getError = store.createSelector(getProfilePageState, stateGetError);
    var ɵ2$1 = function (state) { return state.success; };
    var getSuccess = store.createSelector(getProfilePageState, ɵ2$1);

    var ProfileStore = /** @class */ (function () {
        function ProfileStore(store) {
            this.store = store;
        }
        Object.defineProperty(ProfileStore.prototype, "Loading$", {
            get: function () { return this.store.select(getIsLoading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ProfileStore.prototype, "Error$", {
            get: function () { return this.store.select(getError); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ProfileStore.prototype, "Success$", {
            get: function () { return this.store.select(getSuccess); },
            enumerable: true,
            configurable: true
        });
        ProfileStore.prototype.LoadProfile = function () {
            return this.store.dispatch(LoadProfileBeginAction());
        };
        Object.defineProperty(ProfileStore.prototype, "Profile$", {
            get: function () {
                return this.store.select(getProfile);
            },
            enumerable: true,
            configurable: true
        });
        ProfileStore.prototype.UpdateProfile = function (profileForm) {
            return this.store.dispatch(UpdateProfileBeginAction({ profileForm: profileForm }));
        };
        ProfileStore.ctorParameters = function () { return [
            { type: store.Store }
        ]; };
        ProfileStore = __decorate([
            core.Injectable()
        ], ProfileStore);
        return ProfileStore;
    }());

    var ProfileCoreModule = /** @class */ (function () {
        function ProfileCoreModule() {
        }
        ProfileCoreModule_1 = ProfileCoreModule;
        ProfileCoreModule.forRoot = function (config) {
            return {
                ngModule: ProfileCoreModule_1,
                providers: __spread([
                    { provide: PROFILE_SERVICE, useClass: ProfileService },
                    { provide: PROFILE_REPOSITORY, useClass: ProfileRepository }
                ], config.providers, [
                    ProfileStore
                ])
            };
        };
        var ProfileCoreModule_1;
        ProfileCoreModule = ProfileCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [],
                imports: [
                    http.HttpClientModule,
                    store.StoreModule.forFeature('profile', profileReducer),
                    effects.EffectsModule.forFeature([ProfileEffects]),
                ],
                exports: []
            })
        ], ProfileCoreModule);
        return ProfileCoreModule;
    }());

    exports.LoadProfileBeginAction = LoadProfileBeginAction;
    exports.LoadProfileFailAction = LoadProfileFailAction;
    exports.LoadProfileSuccessAction = LoadProfileSuccessAction;
    exports.PROFILE_REPOSITORY = PROFILE_REPOSITORY;
    exports.PROFILE_SERVICE = PROFILE_SERVICE;
    exports.ProfileCoreModule = ProfileCoreModule;
    exports.ProfileEffects = ProfileEffects;
    exports.ProfileModel = ProfileModel;
    exports.ProfileRepository = ProfileRepository;
    exports.ProfileService = ProfileService;
    exports.ProfileStore = ProfileStore;
    exports.UpdateProfileBeginAction = UpdateProfileBeginAction;
    exports.UpdateProfileFailAction = UpdateProfileFailAction;
    exports.UpdateProfileSuccessAction = UpdateProfileSuccessAction;
    exports.getError = getError;
    exports.getIsLoading = getIsLoading;
    exports.getProfile = getProfile;
    exports.getProfilePageState = getProfilePageState;
    exports.getProfileState = getProfileState;
    exports.getSuccess = getSuccess;
    exports.initialState = initialState;
    exports.profileReducer = profileReducer;
    exports.stateGetError = stateGetError;
    exports.stateGetProfile = stateGetProfile;
    exports.ɵ0 = ɵ0$1;
    exports.ɵ1 = ɵ1$1;
    exports.ɵ2 = ɵ2$1;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-profile-core.umd.js.map
