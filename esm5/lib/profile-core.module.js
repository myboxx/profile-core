import { __decorate, __read, __spread } from "tslib";
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PROFILE_REPOSITORY } from './repositories/IProfile.repository';
import { ProfileRepository } from './repositories/profile.repository';
import { PROFILE_SERVICE } from './services/IProfile.service';
import { ProfileService } from './services/profile.service';
import { ProfileEffects } from './state/profile.effects';
import { profileReducer } from './state/profile.reducer';
import { ProfileStore } from './state/profile.store';
var ProfileCoreModule = /** @class */ (function () {
    function ProfileCoreModule() {
    }
    ProfileCoreModule_1 = ProfileCoreModule;
    ProfileCoreModule.forRoot = function (config) {
        return {
            ngModule: ProfileCoreModule_1,
            providers: __spread([
                { provide: PROFILE_SERVICE, useClass: ProfileService },
                { provide: PROFILE_REPOSITORY, useClass: ProfileRepository }
            ], config.providers, [
                ProfileStore
            ])
        };
    };
    var ProfileCoreModule_1;
    ProfileCoreModule = ProfileCoreModule_1 = __decorate([
        NgModule({
            declarations: [],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('profile', profileReducer),
                EffectsModule.forFeature([ProfileEffects]),
            ],
            exports: []
        })
    ], ProfileCoreModule);
    return ProfileCoreModule;
}());
export { ProfileCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS1jb3JlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3Byb2ZpbGUtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9wcm9maWxlLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQXVCLFFBQVEsRUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN4RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDeEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDdEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzlELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQW1CckQ7SUFBQTtJQVlDLENBQUM7MEJBWlcsaUJBQWlCO0lBQ25CLHlCQUFPLEdBQWQsVUFBZSxNQUE4QjtRQUN6QyxPQUFPO1lBQ0gsUUFBUSxFQUFFLG1CQUFpQjtZQUMzQixTQUFTO2dCQUNMLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFO2dCQUN0RCxFQUFFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7ZUFDekQsTUFBTSxDQUFDLFNBQVM7Z0JBQ25CLFlBQVk7Y0FDZjtTQUNKLENBQUM7SUFDTixDQUFDOztJQVhRLGlCQUFpQjtRQWI3QixRQUFRLENBQUM7WUFDUixZQUFZLEVBQUUsRUFFYjtZQUNELE9BQU8sRUFBRTtnQkFDUCxnQkFBZ0I7Z0JBQ2hCLFdBQVcsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQztnQkFDakQsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQzNDO1lBQ0QsT0FBTyxFQUFFLEVBRVI7U0FDRixDQUFDO09BQ1csaUJBQWlCLENBWTVCO0lBQUQsd0JBQUM7Q0FBQSxBQVpGLElBWUU7U0FaVyxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUsIFByb3ZpZGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBFZmZlY3RzTW9kdWxlIH0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XG5pbXBvcnQgeyBTdG9yZU1vZHVsZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IFBST0ZJTEVfUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lQcm9maWxlLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgUHJvZmlsZVJlcG9zaXRvcnkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9wcm9maWxlLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgUFJPRklMRV9TRVJWSUNFIH0gZnJvbSAnLi9zZXJ2aWNlcy9JUHJvZmlsZS5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2ZpbGVTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9wcm9maWxlLnNlcnZpY2UnO1xuaW1wb3J0IHsgUHJvZmlsZUVmZmVjdHMgfSBmcm9tICcuL3N0YXRlL3Byb2ZpbGUuZWZmZWN0cyc7XG5pbXBvcnQgeyBwcm9maWxlUmVkdWNlciB9IGZyb20gJy4vc3RhdGUvcHJvZmlsZS5yZWR1Y2VyJztcbmltcG9ydCB7IFByb2ZpbGVTdG9yZSB9IGZyb20gJy4vc3RhdGUvcHJvZmlsZS5zdG9yZSc7XG5cbmludGVyZmFjZSBNb2R1bGVPcHRpb25zSW50ZXJmYWNlIHtcbiAgICBwcm92aWRlcnM6IFByb3ZpZGVyW107XG59XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuXG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIFN0b3JlTW9kdWxlLmZvckZlYXR1cmUoJ3Byb2ZpbGUnLCBwcm9maWxlUmVkdWNlciksXG4gICAgRWZmZWN0c01vZHVsZS5mb3JGZWF0dXJlKFtQcm9maWxlRWZmZWN0c10pLFxuICBdLFxuICBleHBvcnRzOiBbXG5cbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBQcm9maWxlQ29yZU1vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoY29uZmlnOiBNb2R1bGVPcHRpb25zSW50ZXJmYWNlKTogTW9kdWxlV2l0aFByb3ZpZGVyczxQcm9maWxlQ29yZU1vZHVsZT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmdNb2R1bGU6IFByb2ZpbGVDb3JlTW9kdWxlLFxuICAgICAgICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgICAgICAgICAgeyBwcm92aWRlOiBQUk9GSUxFX1NFUlZJQ0UsIHVzZUNsYXNzOiBQcm9maWxlU2VydmljZSB9LFxuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogUFJPRklMRV9SRVBPU0lUT1JZLCB1c2VDbGFzczogUHJvZmlsZVJlcG9zaXRvcnkgfSxcbiAgICAgICAgICAgICAgICAuLi5jb25maWcucHJvdmlkZXJzLFxuICAgICAgICAgICAgICAgIFByb2ZpbGVTdG9yZVxuICAgICAgICAgICAgXVxuICAgICAgICB9O1xuICAgIH1cbiB9XG4iXX0=