import { __decorate, __param } from "tslib";
import { Inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { ProfileModel } from '../models/profile.model';
import { PROFILE_REPOSITORY } from '../repositories/IProfile.repository';
import * as i0 from "@angular/core";
import * as i1 from "../repositories/IProfile.repository";
var ProfileService = /** @class */ (function () {
    function ProfileService(repository) {
        this.repository = repository;
    }
    ProfileService.prototype.getProfile = function () {
        return this.repository.getProfile().pipe(map(function (response) {
            return ProfileModel.toModel(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    ProfileService.prototype.updateProfile = function (profileForm) {
        return this.repository.updateProfile(profileForm).pipe(map(function (response) {
            return new ProfileModel({
                activeNewsletter: null,
                businessName: response.data.business_name,
                createdAt: null,
                clientEmail: null,
                clientId: null,
                name: response.data.name,
                phoneNumber: null,
                userEmail: response.data.email,
                userId: +response.data.id,
            });
        }), catchError(function (error) {
            throw error;
        }));
    };
    ProfileService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [PROFILE_REPOSITORY,] }] }
    ]; };
    ProfileService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ProfileService_Factory() { return new ProfileService(i0.ɵɵinject(i1.PROFILE_REPOSITORY)); }, token: ProfileService, providedIn: "root" });
    ProfileService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(PROFILE_REPOSITORY))
    ], ProfileService);
    return ProfileService;
}());
export { ProfileService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcHJvZmlsZS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL3Byb2ZpbGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdkQsT0FBTyxFQUtILGtCQUFrQixFQUNyQixNQUFNLHFDQUFxQyxDQUFDOzs7QUFNN0M7SUFFSSx3QkFDd0MsVUFBOEI7UUFBOUIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7SUFDbEUsQ0FBQztJQUVMLG1DQUFVLEdBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUNwQyxHQUFHLENBQUMsVUFBQyxRQUFxRDtZQUN0RCxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxVQUFBLEtBQUs7WUFDWixNQUFNLEtBQUssQ0FBQztRQUNoQixDQUFDLENBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQztJQUVELHNDQUFhLEdBQWIsVUFBYyxXQUFrQztRQUM1QyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FDbEQsR0FBRyxDQUFDLFVBQUMsUUFBd0Q7WUFFekQsT0FBTyxJQUFJLFlBQVksQ0FBQztnQkFDcEIsZ0JBQWdCLEVBQUUsSUFBSTtnQkFDdEIsWUFBWSxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYTtnQkFDekMsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsV0FBVyxFQUFFLElBQUk7Z0JBQ2pCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUk7Z0JBQ3hCLFdBQVcsRUFBRSxJQUFJO2dCQUNqQixTQUFTLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLO2dCQUM5QixNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7YUFDNUIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLEVBQ0YsVUFBVSxDQUFDLFVBQUEsS0FBSztZQUNaLE1BQU0sS0FBSyxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxDQUNMLENBQUM7SUFDTixDQUFDOztnREFsQ0ksTUFBTSxTQUFDLGtCQUFrQjs7O0lBSHJCLGNBQWM7UUFIMUIsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztRQUlPLFdBQUEsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7T0FIdEIsY0FBYyxDQXNDMUI7eUJBdkREO0NBdURDLEFBdENELElBc0NDO1NBdENZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElIdHRwQmFzaWNSZXNwb25zZSB9IGZyb20gJ0Bib3h4L2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgUHJvZmlsZU1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3Byb2ZpbGUubW9kZWwnO1xuaW1wb3J0IHtcbiAgICBJR2V0UHJvZmlsZURhdGFSZXNwb25zZSxcbiAgICBJUHJvZmlsZVJlcG9zaXRvcnksXG4gICAgSVVwZGF0ZVByb2ZpbGVEYXRhUmVzcG9uc2UsXG4gICAgSVVwZGF0ZVByb2ZpbGVQYXlsb2FkLFxuICAgIFBST0ZJTEVfUkVQT1NJVE9SWVxufSBmcm9tICcuLi9yZXBvc2l0b3JpZXMvSVByb2ZpbGUucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBJUHJvZmlsZVNlcnZpY2UgfSBmcm9tICcuL0lQcm9maWxlLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFByb2ZpbGVTZXJ2aWNlIGltcGxlbWVudHMgSVByb2ZpbGVTZXJ2aWNlPFByb2ZpbGVNb2RlbD57XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChQUk9GSUxFX1JFUE9TSVRPUlkpIHByaXZhdGUgcmVwb3NpdG9yeTogSVByb2ZpbGVSZXBvc2l0b3J5LFxuICAgICkgeyB9XG5cbiAgICBnZXRQcm9maWxlKCk6IE9ic2VydmFibGU8UHJvZmlsZU1vZGVsPiB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlcG9zaXRvcnkuZ2V0UHJvZmlsZSgpLnBpcGUoXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBJSHR0cEJhc2ljUmVzcG9uc2U8SUdldFByb2ZpbGVEYXRhUmVzcG9uc2U+KSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFByb2ZpbGVNb2RlbC50b01vZGVsKHJlc3BvbnNlLmRhdGEpO1xuICAgICAgICAgICAgfSksXG4gICAgICAgICAgICBjYXRjaEVycm9yKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgdXBkYXRlUHJvZmlsZShwcm9maWxlRm9ybTogSVVwZGF0ZVByb2ZpbGVQYXlsb2FkKTogT2JzZXJ2YWJsZTxQcm9maWxlTW9kZWw+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVwb3NpdG9yeS51cGRhdGVQcm9maWxlKHByb2ZpbGVGb3JtKS5waXBlKFxuICAgICAgICAgICAgbWFwKChyZXNwb25zZTogSUh0dHBCYXNpY1Jlc3BvbnNlPElVcGRhdGVQcm9maWxlRGF0YVJlc3BvbnNlPikgPT4ge1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9maWxlTW9kZWwoe1xuICAgICAgICAgICAgICAgICAgICBhY3RpdmVOZXdzbGV0dGVyOiBudWxsLFxuICAgICAgICAgICAgICAgICAgICBidXNpbmVzc05hbWU6IHJlc3BvbnNlLmRhdGEuYnVzaW5lc3NfbmFtZSxcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlZEF0OiBudWxsLFxuICAgICAgICAgICAgICAgICAgICBjbGllbnRFbWFpbDogbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgY2xpZW50SWQ6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IHJlc3BvbnNlLmRhdGEubmFtZSxcbiAgICAgICAgICAgICAgICAgICAgcGhvbmVOdW1iZXI6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIHVzZXJFbWFpbDogcmVzcG9uc2UuZGF0YS5lbWFpbCxcbiAgICAgICAgICAgICAgICAgICAgdXNlcklkOiArcmVzcG9uc2UuZGF0YS5pZCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgY2F0Y2hFcnJvcihlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH1cbn1cbiJdfQ==