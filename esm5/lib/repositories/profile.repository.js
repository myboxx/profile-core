import { __decorate, __param } from "tslib";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
var ProfileRepository = /** @class */ (function () {
    function ProfileRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    ProfileRepository.prototype.getProfile = function () {
        return this.httpClient.get(this.getBaseUrl() + "/get_profile");
    };
    ProfileRepository.prototype.updateProfile = function (profileForm) {
        var params = new HttpParams();
        for (var key in profileForm) {
            if (profileForm.hasOwnProperty(key)) {
                params = params.append(key, profileForm[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update", body);
    };
    ProfileRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/profile";
    };
    ProfileRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    ProfileRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], ProfileRepository);
    return ProfileRepository;
}());
export { ProfileRepository };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5yZXBvc2l0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcHJvZmlsZS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3JlcG9zaXRvcmllcy9wcm9maWxlLnJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBVTlGO0lBRUksMkJBQ3dDLFdBQXFDLEVBQ2pFLFVBQXNCO1FBRE0sZ0JBQVcsR0FBWCxXQUFXLENBQTBCO1FBQ2pFLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDOUIsQ0FBQztJQUVMLHNDQUFVLEdBQVY7UUFFSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFpRCxJQUFJLENBQUMsVUFBVSxFQUFFLGlCQUFjLENBQUMsQ0FBQztJQUNoSCxDQUFDO0lBRUQseUNBQWEsR0FBYixVQUFjLFdBQWtDO1FBRTVDLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFDOUIsS0FBSyxJQUFNLEdBQUcsSUFBSSxXQUFXLEVBQUU7WUFDM0IsSUFBSSxXQUFXLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNqQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDakQ7U0FDSjtRQUNELElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUUvQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFvRCxJQUFJLENBQUMsVUFBVSxFQUFFLFlBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNySCxDQUFDO0lBRU8sc0NBQVUsR0FBbEI7UUFDSSxPQUFVLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLGFBQVEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsZ0JBQWEsQ0FBQztJQUN6RixDQUFDOztnQkF4Qm9ELHdCQUF3Qix1QkFBeEUsTUFBTSxTQUFDLGtCQUFrQjtnQkFDTixVQUFVOztJQUp6QixpQkFBaUI7UUFEN0IsVUFBVSxFQUFFO1FBSUosV0FBQSxNQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtPQUh0QixpQkFBaUIsQ0E0QjdCO0lBQUQsd0JBQUM7Q0FBQSxBQTVCRCxJQTRCQztTQTVCWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBYnN0cmFjdEFwcENvbmZpZ1NlcnZpY2UsIEFQUF9DT05GSUdfU0VSVklDRSwgSUh0dHBCYXNpY1Jlc3BvbnNlIH0gZnJvbSAnQGJveHgvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge1xuICAgIElHZXRQcm9maWxlRGF0YVJlc3BvbnNlLFxuICAgIElQcm9maWxlUmVwb3NpdG9yeSxcbiAgICBJVXBkYXRlUHJvZmlsZURhdGFSZXNwb25zZSxcbiAgICBJVXBkYXRlUHJvZmlsZVBheWxvYWRcbn0gZnJvbSAnLi9JUHJvZmlsZS5yZXBvc2l0b3J5JztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFByb2ZpbGVSZXBvc2l0b3J5IGltcGxlbWVudHMgSVByb2ZpbGVSZXBvc2l0b3J5IHtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBASW5qZWN0KEFQUF9DT05GSUdfU0VSVklDRSkgcHJpdmF0ZSBhcHBTZXR0aW5nczogQWJzdHJhY3RBcHBDb25maWdTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXG4gICAgKSB7IH1cblxuICAgIGdldFByb2ZpbGUoKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SUdldFByb2ZpbGVEYXRhUmVzcG9uc2U+PiB7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8SUh0dHBCYXNpY1Jlc3BvbnNlPElHZXRQcm9maWxlRGF0YVJlc3BvbnNlPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2dldF9wcm9maWxlYCk7XG4gICAgfVxuXG4gICAgdXBkYXRlUHJvZmlsZShwcm9maWxlRm9ybTogSVVwZGF0ZVByb2ZpbGVQYXlsb2FkKTogT2JzZXJ2YWJsZTxJSHR0cEJhc2ljUmVzcG9uc2U8SVVwZGF0ZVByb2ZpbGVEYXRhUmVzcG9uc2U+PiB7XG5cbiAgICAgICAgbGV0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCk7XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIHByb2ZpbGVGb3JtKSB7XG4gICAgICAgICAgICBpZiAocHJvZmlsZUZvcm0uaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgICAgIHBhcmFtcyA9IHBhcmFtcy5hcHBlbmQoa2V5LCBwcm9maWxlRm9ybVtrZXldKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjb25zdCBib2R5ID0gcGFyYW1zLnRvU3RyaW5nKCk7XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0PElIdHRwQmFzaWNSZXNwb25zZTxJVXBkYXRlUHJvZmlsZURhdGFSZXNwb25zZT4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS91cGRhdGVgLCBib2R5KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEJhc2VVcmwoKcKge1xuICAgICAgICByZXR1cm4gYCR7dGhpcy5hcHBTZXR0aW5ncy5iYXNlVXJsKCl9L2FwaS8ke3RoaXMuYXBwU2V0dGluZ3MuaW5zdGFuY2UoKX0vdjEvcHJvZmlsZWA7XG4gICAgfVxufVxuIl19