import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromActions from './profile.actions';
import * as fromSelector from './profile.selectors';
var ProfileStore = /** @class */ (function () {
    function ProfileStore(store) {
        this.store = store;
    }
    Object.defineProperty(ProfileStore.prototype, "Loading$", {
        get: function () { return this.store.select(fromSelector.getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProfileStore.prototype, "Error$", {
        get: function () { return this.store.select(fromSelector.getError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProfileStore.prototype, "Success$", {
        get: function () { return this.store.select(fromSelector.getSuccess); },
        enumerable: true,
        configurable: true
    });
    ProfileStore.prototype.LoadProfile = function () {
        return this.store.dispatch(fromActions.LoadProfileBeginAction());
    };
    Object.defineProperty(ProfileStore.prototype, "Profile$", {
        get: function () {
            return this.store.select(fromSelector.getProfile);
        },
        enumerable: true,
        configurable: true
    });
    ProfileStore.prototype.UpdateProfile = function (profileForm) {
        return this.store.dispatch(fromActions.UpdateProfileBeginAction({ profileForm: profileForm }));
    };
    ProfileStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    ProfileStore = __decorate([
        Injectable()
    ], ProfileStore);
    return ProfileStore;
}());
export { ProfileStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5zdG9yZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3Byb2ZpbGUtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9zdGF0ZS9wcm9maWxlLnN0b3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDcEMsT0FBTyxLQUFLLFdBQVcsTUFBTSxtQkFBbUIsQ0FBQztBQUVqRCxPQUFPLEtBQUssWUFBWSxNQUFNLHFCQUFxQixDQUFDO0FBR3BEO0lBQ0ksc0JBQW9CLEtBQXNDO1FBQXRDLFVBQUssR0FBTCxLQUFLLENBQWlDO0lBQUksQ0FBQztJQUUvRCxzQkFBSSxrQ0FBUTthQUFaLGNBQWlCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFdkUsc0JBQUksZ0NBQU07YUFBVixjQUFlLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFakUsc0JBQUksa0NBQVE7YUFBWixjQUFpQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBRXJFLGtDQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUM7SUFDckUsQ0FBQztJQUVELHNCQUFJLGtDQUFRO2FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RCxDQUFDOzs7T0FBQTtJQUVELG9DQUFhLEdBQWIsVUFBYyxXQUE0QztRQUN0RCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3RGLENBQUM7O2dCQWxCMEIsS0FBSzs7SUFEdkIsWUFBWTtRQUR4QixVQUFVLEVBQUU7T0FDQSxZQUFZLENBb0J4QjtJQUFELG1CQUFDO0NBQUEsQUFwQkQsSUFvQkM7U0FwQlksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN0b3JlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0ICogYXMgZnJvbUFjdGlvbnMgZnJvbSAnLi9wcm9maWxlLmFjdGlvbnMnO1xuaW1wb3J0ICogYXMgZnJvbVJlZHVjZXIgZnJvbSAnLi9wcm9maWxlLnJlZHVjZXInO1xuaW1wb3J0ICogYXMgZnJvbVNlbGVjdG9yIGZyb20gJy4vcHJvZmlsZS5zZWxlY3RvcnMnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUHJvZmlsZVN0b3JlIHtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN0b3JlOiBTdG9yZTxmcm9tUmVkdWNlci5Qcm9maWxlU3RhdGU+KSB7IH1cblxuICAgIGdldCBMb2FkaW5nJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRJc0xvYWRpbmcpOyB9XG5cbiAgICBnZXQgRXJyb3IkKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldEVycm9yKTsgfVxuXG4gICAgZ2V0IFN1Y2Nlc3MkKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldFN1Y2Nlc3MpOyB9XG5cbiAgICBMb2FkUHJvZmlsZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuTG9hZFByb2ZpbGVCZWdpbkFjdGlvbigpKTtcbiAgICB9XG5cbiAgICBnZXQgUHJvZmlsZSQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0UHJvZmlsZSk7XG4gICAgfVxuXG4gICAgVXBkYXRlUHJvZmlsZShwcm9maWxlRm9ybTogeyBuYW1lOiBzdHJpbmcsIGVtYWlsOiBzdHJpbmcgfSkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5kaXNwYXRjaChmcm9tQWN0aW9ucy5VcGRhdGVQcm9maWxlQmVnaW5BY3Rpb24oeyBwcm9maWxlRm9ybSB9KSk7XG4gICAgfVxufVxuIl19