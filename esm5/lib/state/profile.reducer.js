import { __assign } from "tslib";
import { createReducer, on } from '@ngrx/store';
import { ProfileModel } from '../models/profile.model';
import * as fromActions from './profile.actions';
export var initialState = {
    isLoading: false,
    profile: ProfileModel.empty(),
    error: null,
    success: null
};
var ɵ0 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { profile: action.payload, isLoading: false })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { profile: action.response, isLoading: false, success: { after: getSuccessActionType(action.type) } })); };
var reducer = createReducer(initialState, on(fromActions.LoadProfileBeginAction, fromActions.UpdateProfileBeginAction, ɵ0), on(fromActions.LoadProfileSuccessAction, ɵ1), on(fromActions.LoadProfileFailAction, fromActions.UpdateProfileFailAction, ɵ2), on(fromActions.UpdateProfileSuccessAction, ɵ3));
function getErrorActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case fromActions.ProfileActionTypes.LoadProfileFail:
            action = 'GET';
            break;
        case fromActions.ProfileActionTypes.UpdateProfileFail:
            action = 'UPDATE';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case fromActions.ProfileActionTypes.LoadProfileSuccess:
            action = 'GET';
            break;
        case fromActions.ProfileActionTypes.UpdateProfileSuccess:
            action = 'UPDATE';
            break;
    }
    return action;
}
export function profileReducer(state, action) {
    return reducer(state, action);
}
export { ɵ0, ɵ1, ɵ2, ɵ3 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZmlsZS5yZWR1Y2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvcHJvZmlsZS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3N0YXRlL3Byb2ZpbGUucmVkdWNlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUF5QixhQUFhLEVBQUUsRUFBRSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRXZFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEtBQUssV0FBVyxNQUFNLG1CQUFtQixDQUFDO0FBU2pELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBaUI7SUFDdEMsU0FBUyxFQUFFLEtBQUs7SUFDaEIsT0FBTyxFQUFFLFlBQVksQ0FBQyxLQUFLLEVBQUU7SUFDN0IsS0FBSyxFQUFFLElBQUk7SUFDWCxPQUFPLEVBQUUsSUFBSTtDQUNoQixDQUFDO1NBT00sVUFBQyxLQUFLLElBQW1CLE9BQUEsdUJBQ2xCLEtBQUssS0FDUixLQUFLLEVBQUUsSUFBSSxFQUNYLE9BQU8sRUFBRSxJQUFJLEVBQ2IsU0FBUyxFQUFFLElBQUksSUFDakIsRUFMdUIsQ0FLdkIsT0FDbUMsVUFBQyxLQUFLLEVBQUUsTUFBTSxJQUFtQixPQUFBLHVCQUNuRSxLQUFLLEtBQ1IsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPLEVBQ3ZCLFNBQVMsRUFBRSxLQUFLLElBQ2xCLEVBSndFLENBSXhFLE9BSUUsVUFBQyxLQUFLLEVBQUUsTUFBTSxJQUFtQixPQUFBLHVCQUMxQixLQUFLLEtBQ1IsU0FBUyxFQUFFLEtBQUssRUFDaEIsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUN6RSxFQUorQixDQUkvQixPQUVxQyxVQUFDLEtBQUssRUFBRSxNQUFNLElBQW1CLE9BQUEsdUJBQ3JFLEtBQUssS0FDUixPQUFPLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFDeEIsU0FBUyxFQUFFLEtBQUssRUFDaEIsT0FBTyxFQUFFLEVBQUUsS0FBSyxFQUFFLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUN2RCxFQUwwRSxDQUsxRTtBQTlCTixJQUFNLE9BQU8sR0FBZ0MsYUFBYSxDQUN0RCxZQUFZLEVBQ1osRUFBRSxDQUNFLFdBQVcsQ0FBQyxzQkFBc0IsRUFDbEMsV0FBVyxDQUFDLHdCQUF3QixLQU1qQyxFQUNQLEVBQUUsQ0FBQyxXQUFXLENBQUMsd0JBQXdCLEtBSXBDLEVBQ0gsRUFBRSxDQUNFLFdBQVcsQ0FBQyxxQkFBcUIsRUFDakMsV0FBVyxDQUFDLHVCQUF1QixLQUtoQyxFQUVQLEVBQUUsQ0FBQyxXQUFXLENBQUMsMEJBQTBCLEtBS3RDLENBQ04sQ0FBQztBQUVGLFNBQVMsa0JBQWtCLENBQUMsSUFBb0M7SUFFNUQsSUFBSSxNQUFNLEdBQWlDLFNBQVMsQ0FBQztJQUVyRCxRQUFRLElBQUksRUFBRTtRQUNWLEtBQUssV0FBVyxDQUFDLGtCQUFrQixDQUFDLGVBQWU7WUFDL0MsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUFDLE1BQU07UUFDMUIsS0FBSyxXQUFXLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCO1lBQ2pELE1BQU0sR0FBRyxRQUFRLENBQUM7WUFBQyxNQUFNO0tBQ2hDO0lBRUQsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQztBQUVELFNBQVMsb0JBQW9CLENBQUMsSUFBb0M7SUFFOUQsSUFBSSxNQUFNLEdBQWlDLFNBQVMsQ0FBQztJQUVyRCxRQUFRLElBQUksRUFBRTtRQUNWLEtBQUssV0FBVyxDQUFDLGtCQUFrQixDQUFDLGtCQUFrQjtZQUNsRCxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQUMsTUFBTTtRQUMxQixLQUFLLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxvQkFBb0I7WUFDcEQsTUFBTSxHQUFHLFFBQVEsQ0FBQztZQUFDLE1BQU07S0FDaEM7SUFFRCxPQUFPLE1BQU0sQ0FBQztBQUNsQixDQUFDO0FBRUQsTUFBTSxVQUFVLGNBQWMsQ0FBQyxLQUErQixFQUFFLE1BQWM7SUFDMUUsT0FBTyxPQUFPLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQ2xDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY3Rpb24sIEFjdGlvblJlZHVjZXIsIGNyZWF0ZVJlZHVjZXIsIG9uIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgSVByb2ZpbGVTdGF0ZUVycm9yLCBJUHJvZmlsZVN0YXRlU3VjY2VzcyB9IGZyb20gJy4uL2NvcmUvSVN0YXRlRXJyb3JTdWNjZXNzJztcbmltcG9ydCB7IFByb2ZpbGVNb2RlbCB9IGZyb20gJy4uL21vZGVscy9wcm9maWxlLm1vZGVsJztcbmltcG9ydCAqIGFzIGZyb21BY3Rpb25zIGZyb20gJy4vcHJvZmlsZS5hY3Rpb25zJztcblxuZXhwb3J0IGludGVyZmFjZSBQcm9maWxlU3RhdGUge1xuICAgIGlzTG9hZGluZzogYm9vbGVhbjtcbiAgICBwcm9maWxlOiBQcm9maWxlTW9kZWw7XG4gICAgZXJyb3I6IElQcm9maWxlU3RhdGVFcnJvcjtcbiAgICBzdWNjZXNzOiBJUHJvZmlsZVN0YXRlU3VjY2Vzcztcbn1cblxuZXhwb3J0IGNvbnN0IGluaXRpYWxTdGF0ZTogUHJvZmlsZVN0YXRlID0ge1xuICAgIGlzTG9hZGluZzogZmFsc2UsXG4gICAgcHJvZmlsZTogUHJvZmlsZU1vZGVsLmVtcHR5KCksXG4gICAgZXJyb3I6IG51bGwsXG4gICAgc3VjY2VzczogbnVsbFxufTtcblxuY29uc3QgcmVkdWNlcjogQWN0aW9uUmVkdWNlcjxQcm9maWxlU3RhdGU+ID0gY3JlYXRlUmVkdWNlcihcbiAgICBpbml0aWFsU3RhdGUsXG4gICAgb24oXG4gICAgICAgIGZyb21BY3Rpb25zLkxvYWRQcm9maWxlQmVnaW5BY3Rpb24sXG4gICAgICAgIGZyb21BY3Rpb25zLlVwZGF0ZVByb2ZpbGVCZWdpbkFjdGlvbixcbiAgICAgICAgKHN0YXRlKTogUHJvZmlsZVN0YXRlID0+ICh7XG4gICAgICAgICAgICAuLi5zdGF0ZSxcbiAgICAgICAgICAgIGVycm9yOiBudWxsLFxuICAgICAgICAgICAgc3VjY2VzczogbnVsbCxcbiAgICAgICAgICAgIGlzTG9hZGluZzogdHJ1ZSxcbiAgICAgICAgfSkpLFxuICAgIG9uKGZyb21BY3Rpb25zLkxvYWRQcm9maWxlU3VjY2Vzc0FjdGlvbiwgKHN0YXRlLCBhY3Rpb24pOiBQcm9maWxlU3RhdGUgPT4gKHtcbiAgICAgICAgLi4uc3RhdGUsXG4gICAgICAgIHByb2ZpbGU6IGFjdGlvbi5wYXlsb2FkLFxuICAgICAgICBpc0xvYWRpbmc6IGZhbHNlLFxuICAgIH0pKSxcbiAgICBvbihcbiAgICAgICAgZnJvbUFjdGlvbnMuTG9hZFByb2ZpbGVGYWlsQWN0aW9uLFxuICAgICAgICBmcm9tQWN0aW9ucy5VcGRhdGVQcm9maWxlRmFpbEFjdGlvbixcbiAgICAgICAgKHN0YXRlLCBhY3Rpb24pOiBQcm9maWxlU3RhdGUgPT4gKHtcbiAgICAgICAgICAgIC4uLnN0YXRlLFxuICAgICAgICAgICAgaXNMb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgICAgIGVycm9yOiB7IGFmdGVyOiBnZXRFcnJvckFjdGlvblR5cGUoYWN0aW9uLnR5cGUpLCBlcnJvcjogYWN0aW9uLmVycm9ycyB9XG4gICAgICAgIH0pKSxcblxuICAgIG9uKGZyb21BY3Rpb25zLlVwZGF0ZVByb2ZpbGVTdWNjZXNzQWN0aW9uLCAoc3RhdGUsIGFjdGlvbik6IFByb2ZpbGVTdGF0ZSA9PiAoe1xuICAgICAgICAuLi5zdGF0ZSxcbiAgICAgICAgcHJvZmlsZTogYWN0aW9uLnJlc3BvbnNlLFxuICAgICAgICBpc0xvYWRpbmc6IGZhbHNlLFxuICAgICAgICBzdWNjZXNzOiB7IGFmdGVyOiBnZXRTdWNjZXNzQWN0aW9uVHlwZShhY3Rpb24udHlwZSkgfVxuICAgIH0pKVxuKTtcblxuZnVuY3Rpb24gZ2V0RXJyb3JBY3Rpb25UeXBlKHR5cGU6IGZyb21BY3Rpb25zLlByb2ZpbGVBY3Rpb25UeXBlcykge1xuXG4gICAgbGV0IGFjdGlvbjogJ0dFVCcgfCAnVVBEQVRFJyB8ICdVTktOT1dOJyA9ICdVTktOT1dOJztcblxuICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICBjYXNlIGZyb21BY3Rpb25zLlByb2ZpbGVBY3Rpb25UeXBlcy5Mb2FkUHJvZmlsZUZhaWw6XG4gICAgICAgICAgICBhY3Rpb24gPSAnR0VUJzsgYnJlYWs7XG4gICAgICAgIGNhc2UgZnJvbUFjdGlvbnMuUHJvZmlsZUFjdGlvblR5cGVzLlVwZGF0ZVByb2ZpbGVGYWlsOlxuICAgICAgICAgICAgYWN0aW9uID0gJ1VQREFURSc7IGJyZWFrO1xuICAgIH1cblxuICAgIHJldHVybiBhY3Rpb247XG59XG5cbmZ1bmN0aW9uIGdldFN1Y2Nlc3NBY3Rpb25UeXBlKHR5cGU6IGZyb21BY3Rpb25zLlByb2ZpbGVBY3Rpb25UeXBlcykge1xuXG4gICAgbGV0IGFjdGlvbjogJ0dFVCcgfCAnVVBEQVRFJyB8ICdVTktOT1dOJyA9ICdVTktOT1dOJztcblxuICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICBjYXNlIGZyb21BY3Rpb25zLlByb2ZpbGVBY3Rpb25UeXBlcy5Mb2FkUHJvZmlsZVN1Y2Nlc3M6XG4gICAgICAgICAgICBhY3Rpb24gPSAnR0VUJzsgYnJlYWs7XG4gICAgICAgIGNhc2UgZnJvbUFjdGlvbnMuUHJvZmlsZUFjdGlvblR5cGVzLlVwZGF0ZVByb2ZpbGVTdWNjZXNzOlxuICAgICAgICAgICAgYWN0aW9uID0gJ1VQREFURSc7IGJyZWFrO1xuICAgIH1cblxuICAgIHJldHVybiBhY3Rpb247XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBwcm9maWxlUmVkdWNlcihzdGF0ZTogUHJvZmlsZVN0YXRlIHwgdW5kZWZpbmVkLCBhY3Rpb246IEFjdGlvbikge1xuICAgIHJldHVybiByZWR1Y2VyKHN0YXRlLCBhY3Rpb24pO1xufVxuIl19