import { __decorate, __param } from 'tslib';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

const PROFILE_REPOSITORY = new InjectionToken('profileRepository');

let ProfileRepository = class ProfileRepository {
    constructor(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    getProfile() {
        return this.httpClient.get(`${this.getBaseUrl()}/get_profile`);
    }
    updateProfile(profileForm) {
        let params = new HttpParams();
        for (const key in profileForm) {
            if (profileForm.hasOwnProperty(key)) {
                params = params.append(key, profileForm[key]);
            }
        }
        const body = params.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/update`, body);
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/profile`;
    }
};
ProfileRepository.ctorParameters = () => [
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
    { type: HttpClient }
];
ProfileRepository = __decorate([
    Injectable(),
    __param(0, Inject(APP_CONFIG_SERVICE))
], ProfileRepository);

const PROFILE_SERVICE = new InjectionToken('profileService');

class ProfileModel {
    constructor(data) {
        this.businessName = data.businessName;
        this.activeNewsletter = data.activeNewsletter;
        this.createdAt = data.createdAt;
        this.clientEmail = data.clientEmail;
        this.clientId = data.clientId;
        this.name = data.name;
        this.phoneNumber = data.phoneNumber;
        this.userEmail = data.userEmail;
        this.userId = data.userId;
    }
    static toModel(data) {
        return new ProfileModel({
            activeNewsletter: data.active_newsletter === '1',
            businessName: data.business_name,
            createdAt: data.created_at,
            clientEmail: data.client_email,
            clientId: +data.client_id,
            name: data.name,
            phoneNumber: data.phone_number !== 'null' ? data.phone_number : null,
            userEmail: data.user_email,
            userId: +data.user_id
        });
    }
    static empty() {
        return new ProfileModel({
            activeNewsletter: null,
            businessName: null,
            createdAt: null,
            clientEmail: null,
            clientId: null,
            name: null,
            phoneNumber: null,
            userEmail: null,
            userId: null
        });
    }
}

let ProfileService = class ProfileService {
    constructor(repository) {
        this.repository = repository;
    }
    getProfile() {
        return this.repository.getProfile().pipe(map((response) => {
            return ProfileModel.toModel(response.data);
        }), catchError(error => {
            throw error;
        }));
    }
    updateProfile(profileForm) {
        return this.repository.updateProfile(profileForm).pipe(map((response) => {
            return new ProfileModel({
                activeNewsletter: null,
                businessName: response.data.business_name,
                createdAt: null,
                clientEmail: null,
                clientId: null,
                name: response.data.name,
                phoneNumber: null,
                userEmail: response.data.email,
                userId: +response.data.id,
            });
        }), catchError(error => {
            throw error;
        }));
    }
};
ProfileService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [PROFILE_REPOSITORY,] }] }
];
ProfileService.ɵprov = ɵɵdefineInjectable({ factory: function ProfileService_Factory() { return new ProfileService(ɵɵinject(PROFILE_REPOSITORY)); }, token: ProfileService, providedIn: "root" });
ProfileService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(PROFILE_REPOSITORY))
], ProfileService);

var ProfileActionTypes;
(function (ProfileActionTypes) {
    ProfileActionTypes["LoadProfileBegin"] = "[PROFILE] Load Profile Begin";
    ProfileActionTypes["LoadProfileSuccess"] = "[PROFILE] Load Profile Success";
    ProfileActionTypes["LoadProfileFail"] = "[PROFILE] Load Profile Fail";
    ProfileActionTypes["UpdateProfileBegin"] = "[PROFILE] Update Profile Begin";
    ProfileActionTypes["UpdateProfileSuccess"] = "[PROFILE] Update Profile Success";
    ProfileActionTypes["UpdateProfileFail"] = "[PROFILE] Update Profile Fail";
})(ProfileActionTypes || (ProfileActionTypes = {}));
// GET
const LoadProfileBeginAction = createAction(ProfileActionTypes.LoadProfileBegin);
const LoadProfileSuccessAction = createAction(ProfileActionTypes.LoadProfileSuccess, props());
const LoadProfileFailAction = createAction(ProfileActionTypes.LoadProfileFail, props());
// UPDATE
const UpdateProfileBeginAction = createAction(ProfileActionTypes.UpdateProfileBegin, props());
const UpdateProfileSuccessAction = createAction(ProfileActionTypes.UpdateProfileSuccess, props());
const UpdateProfileFailAction = createAction(ProfileActionTypes.UpdateProfileFail, props());

let ProfileEffects = class ProfileEffects {
    constructor(actions$, service) {
        this.actions$ = actions$;
        this.service = service;
        this.loadProfile$ = createEffect(() => this.actions$.pipe(ofType(ProfileActionTypes.LoadProfileBegin), switchMap(() => {
            return this.service.getProfile().pipe(map(profile => LoadProfileSuccessAction({ payload: profile })), catchError(error => {
                console.error('Couldn\'t load Profile');
                return of(LoadProfileFailAction({ errors: error }));
            }));
        })));
        this.updateProfile$ = createEffect(() => this.actions$.pipe(ofType(ProfileActionTypes.UpdateProfileBegin), switchMap((action) => {
            return this.service.updateProfile(action.profileForm).pipe(map(response => UpdateProfileSuccessAction({ response })), catchError(error => {
                console.error('Couldn\'t UPDATE Profile', error);
                return of(UpdateProfileFailAction({ errors: error }));
            }));
        })));
    }
};
ProfileEffects.ctorParameters = () => [
    { type: Actions },
    { type: undefined, decorators: [{ type: Inject, args: [PROFILE_SERVICE,] }] }
];
ProfileEffects = __decorate([
    Injectable(),
    __param(1, Inject(PROFILE_SERVICE))
], ProfileEffects);

const initialState = {
    isLoading: false,
    profile: ProfileModel.empty(),
    error: null,
    success: null
};
const ɵ0 = (state) => (Object.assign(Object.assign({}, state), { error: null, success: null, isLoading: true })), ɵ1 = (state, action) => (Object.assign(Object.assign({}, state), { profile: action.payload, isLoading: false })), ɵ2 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })), ɵ3 = (state, action) => (Object.assign(Object.assign({}, state), { profile: action.response, isLoading: false, success: { after: getSuccessActionType(action.type) } }));
const reducer = createReducer(initialState, on(LoadProfileBeginAction, UpdateProfileBeginAction, ɵ0), on(LoadProfileSuccessAction, ɵ1), on(LoadProfileFailAction, UpdateProfileFailAction, ɵ2), on(UpdateProfileSuccessAction, ɵ3));
function getErrorActionType(type) {
    let action = 'UNKNOWN';
    switch (type) {
        case ProfileActionTypes.LoadProfileFail:
            action = 'GET';
            break;
        case ProfileActionTypes.UpdateProfileFail:
            action = 'UPDATE';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    let action = 'UNKNOWN';
    switch (type) {
        case ProfileActionTypes.LoadProfileSuccess:
            action = 'GET';
            break;
        case ProfileActionTypes.UpdateProfileSuccess:
            action = 'UPDATE';
            break;
    }
    return action;
}
function profileReducer(state, action) {
    return reducer(state, action);
}

const getProfileState = createFeatureSelector('profile');
const ɵ0$1 = state => state;
const getProfilePageState = createSelector(getProfileState, ɵ0$1);
const stateGetIsLoading = (state) => state.isLoading;
const ɵ1$1 = stateGetIsLoading;
const stateGetProfile = (state) => state.profile;
const stateGetError = (state) => state.error;
const getIsLoading = createSelector(getProfilePageState, stateGetIsLoading);
const getProfile = createSelector(getProfilePageState, stateGetProfile);
const getError = createSelector(getProfilePageState, stateGetError);
const ɵ2$1 = state => state.success;
const getSuccess = createSelector(getProfilePageState, ɵ2$1);

let ProfileStore = class ProfileStore {
    constructor(store) {
        this.store = store;
    }
    get Loading$() { return this.store.select(getIsLoading); }
    get Error$() { return this.store.select(getError); }
    get Success$() { return this.store.select(getSuccess); }
    LoadProfile() {
        return this.store.dispatch(LoadProfileBeginAction());
    }
    get Profile$() {
        return this.store.select(getProfile);
    }
    UpdateProfile(profileForm) {
        return this.store.dispatch(UpdateProfileBeginAction({ profileForm }));
    }
};
ProfileStore.ctorParameters = () => [
    { type: Store }
];
ProfileStore = __decorate([
    Injectable()
], ProfileStore);

var ProfileCoreModule_1;
let ProfileCoreModule = ProfileCoreModule_1 = class ProfileCoreModule {
    static forRoot(config) {
        return {
            ngModule: ProfileCoreModule_1,
            providers: [
                { provide: PROFILE_SERVICE, useClass: ProfileService },
                { provide: PROFILE_REPOSITORY, useClass: ProfileRepository },
                ...config.providers,
                ProfileStore
            ]
        };
    }
};
ProfileCoreModule = ProfileCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('profile', profileReducer),
            EffectsModule.forFeature([ProfileEffects]),
        ],
        exports: []
    })
], ProfileCoreModule);

/*
 * Public API Surface of profile-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { LoadProfileBeginAction, LoadProfileFailAction, LoadProfileSuccessAction, PROFILE_REPOSITORY, PROFILE_SERVICE, ProfileActionTypes, ProfileCoreModule, ProfileEffects, ProfileModel, ProfileRepository, ProfileService, ProfileStore, UpdateProfileBeginAction, UpdateProfileFailAction, UpdateProfileSuccessAction, getError, getIsLoading, getProfile, getProfilePageState, getProfileState, getSuccess, initialState, profileReducer, stateGetError, stateGetProfile, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2 };
//# sourceMappingURL=boxx-profile-core.js.map
