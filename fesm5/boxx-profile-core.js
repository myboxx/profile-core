import { __decorate, __param, __assign, __spread } from 'tslib';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

var PROFILE_REPOSITORY = new InjectionToken('profileRepository');

var ProfileRepository = /** @class */ (function () {
    function ProfileRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    ProfileRepository.prototype.getProfile = function () {
        return this.httpClient.get(this.getBaseUrl() + "/get_profile");
    };
    ProfileRepository.prototype.updateProfile = function (profileForm) {
        var params = new HttpParams();
        for (var key in profileForm) {
            if (profileForm.hasOwnProperty(key)) {
                params = params.append(key, profileForm[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update", body);
    };
    ProfileRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/profile";
    };
    ProfileRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    ProfileRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], ProfileRepository);
    return ProfileRepository;
}());

var PROFILE_SERVICE = new InjectionToken('profileService');

var ProfileModel = /** @class */ (function () {
    function ProfileModel(data) {
        this.businessName = data.businessName;
        this.activeNewsletter = data.activeNewsletter;
        this.createdAt = data.createdAt;
        this.clientEmail = data.clientEmail;
        this.clientId = data.clientId;
        this.name = data.name;
        this.phoneNumber = data.phoneNumber;
        this.userEmail = data.userEmail;
        this.userId = data.userId;
    }
    ProfileModel.toModel = function (data) {
        return new ProfileModel({
            activeNewsletter: data.active_newsletter === '1',
            businessName: data.business_name,
            createdAt: data.created_at,
            clientEmail: data.client_email,
            clientId: +data.client_id,
            name: data.name,
            phoneNumber: data.phone_number !== 'null' ? data.phone_number : null,
            userEmail: data.user_email,
            userId: +data.user_id
        });
    };
    ProfileModel.empty = function () {
        return new ProfileModel({
            activeNewsletter: null,
            businessName: null,
            createdAt: null,
            clientEmail: null,
            clientId: null,
            name: null,
            phoneNumber: null,
            userEmail: null,
            userId: null
        });
    };
    return ProfileModel;
}());

var ProfileService = /** @class */ (function () {
    function ProfileService(repository) {
        this.repository = repository;
    }
    ProfileService.prototype.getProfile = function () {
        return this.repository.getProfile().pipe(map(function (response) {
            return ProfileModel.toModel(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    ProfileService.prototype.updateProfile = function (profileForm) {
        return this.repository.updateProfile(profileForm).pipe(map(function (response) {
            return new ProfileModel({
                activeNewsletter: null,
                businessName: response.data.business_name,
                createdAt: null,
                clientEmail: null,
                clientId: null,
                name: response.data.name,
                phoneNumber: null,
                userEmail: response.data.email,
                userId: +response.data.id,
            });
        }), catchError(function (error) {
            throw error;
        }));
    };
    ProfileService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [PROFILE_REPOSITORY,] }] }
    ]; };
    ProfileService.ɵprov = ɵɵdefineInjectable({ factory: function ProfileService_Factory() { return new ProfileService(ɵɵinject(PROFILE_REPOSITORY)); }, token: ProfileService, providedIn: "root" });
    ProfileService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(PROFILE_REPOSITORY))
    ], ProfileService);
    return ProfileService;
}());

var ProfileActionTypes;
(function (ProfileActionTypes) {
    ProfileActionTypes["LoadProfileBegin"] = "[PROFILE] Load Profile Begin";
    ProfileActionTypes["LoadProfileSuccess"] = "[PROFILE] Load Profile Success";
    ProfileActionTypes["LoadProfileFail"] = "[PROFILE] Load Profile Fail";
    ProfileActionTypes["UpdateProfileBegin"] = "[PROFILE] Update Profile Begin";
    ProfileActionTypes["UpdateProfileSuccess"] = "[PROFILE] Update Profile Success";
    ProfileActionTypes["UpdateProfileFail"] = "[PROFILE] Update Profile Fail";
})(ProfileActionTypes || (ProfileActionTypes = {}));
// GET
var LoadProfileBeginAction = createAction(ProfileActionTypes.LoadProfileBegin);
var LoadProfileSuccessAction = createAction(ProfileActionTypes.LoadProfileSuccess, props());
var LoadProfileFailAction = createAction(ProfileActionTypes.LoadProfileFail, props());
// UPDATE
var UpdateProfileBeginAction = createAction(ProfileActionTypes.UpdateProfileBegin, props());
var UpdateProfileSuccessAction = createAction(ProfileActionTypes.UpdateProfileSuccess, props());
var UpdateProfileFailAction = createAction(ProfileActionTypes.UpdateProfileFail, props());

var ProfileEffects = /** @class */ (function () {
    function ProfileEffects(actions$, service) {
        var _this = this;
        this.actions$ = actions$;
        this.service = service;
        this.loadProfile$ = createEffect(function () { return _this.actions$.pipe(ofType(ProfileActionTypes.LoadProfileBegin), switchMap(function () {
            return _this.service.getProfile().pipe(map(function (profile) { return LoadProfileSuccessAction({ payload: profile }); }), catchError(function (error) {
                console.error('Couldn\'t load Profile');
                return of(LoadProfileFailAction({ errors: error }));
            }));
        })); });
        this.updateProfile$ = createEffect(function () { return _this.actions$.pipe(ofType(ProfileActionTypes.UpdateProfileBegin), switchMap(function (action) {
            return _this.service.updateProfile(action.profileForm).pipe(map(function (response) { return UpdateProfileSuccessAction({ response: response }); }), catchError(function (error) {
                console.error('Couldn\'t UPDATE Profile', error);
                return of(UpdateProfileFailAction({ errors: error }));
            }));
        })); });
    }
    ProfileEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: undefined, decorators: [{ type: Inject, args: [PROFILE_SERVICE,] }] }
    ]; };
    ProfileEffects = __decorate([
        Injectable(),
        __param(1, Inject(PROFILE_SERVICE))
    ], ProfileEffects);
    return ProfileEffects;
}());

var initialState = {
    isLoading: false,
    profile: ProfileModel.empty(),
    error: null,
    success: null
};
var ɵ0 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { profile: action.payload, isLoading: false })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getErrorActionType(action.type), error: action.errors } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { profile: action.response, isLoading: false, success: { after: getSuccessActionType(action.type) } })); };
var reducer = createReducer(initialState, on(LoadProfileBeginAction, UpdateProfileBeginAction, ɵ0), on(LoadProfileSuccessAction, ɵ1), on(LoadProfileFailAction, UpdateProfileFailAction, ɵ2), on(UpdateProfileSuccessAction, ɵ3));
function getErrorActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case ProfileActionTypes.LoadProfileFail:
            action = 'GET';
            break;
        case ProfileActionTypes.UpdateProfileFail:
            action = 'UPDATE';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case ProfileActionTypes.LoadProfileSuccess:
            action = 'GET';
            break;
        case ProfileActionTypes.UpdateProfileSuccess:
            action = 'UPDATE';
            break;
    }
    return action;
}
function profileReducer(state, action) {
    return reducer(state, action);
}

var getProfileState = createFeatureSelector('profile');
var ɵ0$1 = function (state) { return state; };
var getProfilePageState = createSelector(getProfileState, ɵ0$1);
var stateGetIsLoading = function (state) { return state.isLoading; };
var ɵ1$1 = stateGetIsLoading;
var stateGetProfile = function (state) { return state.profile; };
var stateGetError = function (state) { return state.error; };
var getIsLoading = createSelector(getProfilePageState, stateGetIsLoading);
var getProfile = createSelector(getProfilePageState, stateGetProfile);
var getError = createSelector(getProfilePageState, stateGetError);
var ɵ2$1 = function (state) { return state.success; };
var getSuccess = createSelector(getProfilePageState, ɵ2$1);

var ProfileStore = /** @class */ (function () {
    function ProfileStore(store) {
        this.store = store;
    }
    Object.defineProperty(ProfileStore.prototype, "Loading$", {
        get: function () { return this.store.select(getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProfileStore.prototype, "Error$", {
        get: function () { return this.store.select(getError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProfileStore.prototype, "Success$", {
        get: function () { return this.store.select(getSuccess); },
        enumerable: true,
        configurable: true
    });
    ProfileStore.prototype.LoadProfile = function () {
        return this.store.dispatch(LoadProfileBeginAction());
    };
    Object.defineProperty(ProfileStore.prototype, "Profile$", {
        get: function () {
            return this.store.select(getProfile);
        },
        enumerable: true,
        configurable: true
    });
    ProfileStore.prototype.UpdateProfile = function (profileForm) {
        return this.store.dispatch(UpdateProfileBeginAction({ profileForm: profileForm }));
    };
    ProfileStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    ProfileStore = __decorate([
        Injectable()
    ], ProfileStore);
    return ProfileStore;
}());

var ProfileCoreModule = /** @class */ (function () {
    function ProfileCoreModule() {
    }
    ProfileCoreModule_1 = ProfileCoreModule;
    ProfileCoreModule.forRoot = function (config) {
        return {
            ngModule: ProfileCoreModule_1,
            providers: __spread([
                { provide: PROFILE_SERVICE, useClass: ProfileService },
                { provide: PROFILE_REPOSITORY, useClass: ProfileRepository }
            ], config.providers, [
                ProfileStore
            ])
        };
    };
    var ProfileCoreModule_1;
    ProfileCoreModule = ProfileCoreModule_1 = __decorate([
        NgModule({
            declarations: [],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('profile', profileReducer),
                EffectsModule.forFeature([ProfileEffects]),
            ],
            exports: []
        })
    ], ProfileCoreModule);
    return ProfileCoreModule;
}());

/*
 * Public API Surface of profile-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { LoadProfileBeginAction, LoadProfileFailAction, LoadProfileSuccessAction, PROFILE_REPOSITORY, PROFILE_SERVICE, ProfileActionTypes, ProfileCoreModule, ProfileEffects, ProfileModel, ProfileRepository, ProfileService, ProfileStore, UpdateProfileBeginAction, UpdateProfileFailAction, UpdateProfileSuccessAction, getError, getIsLoading, getProfile, getProfilePageState, getProfileState, getSuccess, initialState, profileReducer, stateGetError, stateGetProfile, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2 };
//# sourceMappingURL=boxx-profile-core.js.map
